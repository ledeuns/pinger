package main

import(
	"fmt"
	"net"
	"time"
	"flag"
	"encoding/json"
	"github.com/go-ping/ping"
)

type Results struct {
	Name string
	IP net.IP
	PacketsSent int
	PacketsLost int
	MinRtt time.Duration
	AvgRtt time.Duration
	MaxRtt time.Duration
}

func printresult_json(stats []Results) {
	prettyJSON, _ := json.MarshalIndent(stats, "", "  ")
	fmt.Printf("%s\n", prettyJSON)
}

func printresult_default(stats []Results) {
	for _, stat := range stats {
		fmt.Printf("%s (%d/%d) : min: %v, avg: %v, max %v\n",
		    stat.Name, stat.PacketsSent-stat.PacketsLost,
		    stat.PacketsSent, stat.MinRtt, stat.AvgRtt, stat.MaxRtt)
	}
}

func printresult_csv(stats []Results) {
	fmt.Printf("site;ip;sent;loss;min;avg;max;\n")
	for _, stat := range stats {
		fmt.Printf("%s;%s;%d;%d;%d;%d;%d;\n", stat.Name, stat.IP,
		    stat.PacketsSent, stat.PacketsLost,
		    stat.MinRtt, stat.AvgRtt, stat.MaxRtt)
	}
}

func printresult(f string, stats []Results) {
	switch f {
	case "json":
		printresult_json(stats)
	case "csv":
		printresult_csv(stats)
	default:
		printresult_default(stats)
	}
}

func main() {
	results := []Results{}
	outformat := "default"

	formatj := flag.Bool("j", false, "Output to JSON")
	formatc := flag.Bool("c", false, "Output to CSV")
	ip6 := flag.Bool("6", false, "Force IPv6")
	ip4 := flag.Bool("4", false, "Force IPv4")
	flag.Usage = func() {
		fmt.Printf("pinger [-46cj] host1 [host2] [...]\n")
	}
	flag.Parse()

	if *formatj == true {
		outformat = "json"
	}
	if *formatc == true {
		outformat = "csv"
	}

	narg := flag.NArg()
	if narg == 0 {
		flag.Usage()
		return
	}

	for arg := narg; arg >= 0; arg-- {
		validhost := true
		pinger := ping.New(flag.Arg(arg))
		pinger.Count = 5
		pinger.Interval = time.Millisecond*100
		pinger.Timeout = time.Millisecond*1000
		pinger.SetPrivileged(true)

		if *ip6 == true {
			pinger.SetNetwork("ip6")
		} else if *ip4 == true {
			pinger.SetNetwork("ip4")
		} else {
			pinger.SetNetwork("ip6")
			err := pinger.Resolve()
			if err != nil {
				pinger.SetNetwork("ip4")
			}
		}
		err := pinger.Resolve()
		if err != nil {
			validhost = false;
		}

		if validhost == true {
			var result Results
			pinger.Run()
			stats := pinger.Statistics()
			result.Name = stats.Addr
			result.IP = stats.IPAddr.IP
			result.PacketsSent = stats.PacketsSent
			result.PacketsLost = stats.PacketsSent-stats.PacketsRecv
			result.MinRtt = stats.MinRtt
			result.AvgRtt = stats.AvgRtt
			result.MaxRtt = stats.MaxRtt
			results = append(results, result)
		}
	}
	printresult(outformat, results)
}
